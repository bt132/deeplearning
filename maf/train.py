import torch
import numpy as np
from torchvision import transforms, datasets
from maf import MAF
from made import MADE
from datasets.data_loaders import get_data, get_data_loaders
from utils.train import train_one_epoch_maf


# --------- SET PARAMETERS ----------
model_name = "maf"  # 'MAF' or 'MADE'
dataset_name = "mnist"
batch_size = 256
n_mades = 5
hidden_dims = [512]
lr = 1e-4
random_order = False
patience = 30  # For early stopping
seed = 290713
plot = True
max_epochs = 1000
# -----------------------------------

transform = transforms.Compose(
    [
        # convert PIL image to tensor:
        transforms.ToTensor(),
        transforms.Resize(7),
        transforms.Lambda(lambda x: torch.flatten(x)),
    ]
)
# Get dataset.
dataset = datasets.MNIST(
    root="./data/mnist", train=True, transform=transform, download=True
)
# Get data loaders.
train_loader = torch.utils.data.DataLoader(
    dataset=dataset, batch_size=batch_size, shuffle=True, pin_memory=True
)
# Get model.
n_in = 49
model = MAF(n_in, n_mades, hidden_dims)
# Get optimiser.
optimiser = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=1e-6)

# Format name of model save file.
save_name = f"{model_name}_{dataset_name}"
# Initialise list for plotting.

# Training loop.
for epoch in range(1, max_epochs):
    print(f"---epoch {epoch}---")
    train_loss = train_one_epoch_maf(model, epoch, optimiser, train_loader)

    # Early stopping. Save model on each epoch with improvement.
    if epoch % 1 == 0:
        torch.save(
            model.state_dict(), "model_saves/" + save_name + f"_{epoch}.pt"
        )  # Will print a UserWarning 1st epoch.
        print("model saved")
