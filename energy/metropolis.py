import numpy as np
import torch


def sample_metropolis_hasting(x, step_size, model, device):
    x_prev = x
    for _ in range(0, step_size):
        u = torch.rand(1).item()
        x_star = torch.normal(x_prev, 1).to(device)
        with torch.no_grad():
            A = min(
                1,
                (np.exp(-model((x_star)).mean().item()))
                / (np.exp(-model((x_prev)).mean().item())),
            )
            if u < A:
                x_prev = x_star
            else:
                pass
    return x_prev
