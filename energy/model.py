import torch
import torch.nn as nn

## image shape 16x16


class EnergyModel(nn.Module):

    def __init__(self, layers=8, kernel=5, channels=64):
        super().__init__()
        self.layers = layers
        self.kernel = kernel
        self.channels = channels

        models = []
        models.append(nn.Conv2d(1, channels, kernel, 1, kernel // 2, bias=False))
        models.append(nn.BatchNorm2d(channels))
        models.append(nn.ReLU(True))

        for i in range(0, layers - 1):
            models.append(
                nn.Conv2d(channels, channels, kernel, 1, kernel // 2, bias=False)
            )
            models.append(nn.BatchNorm2d(channels))
            models.append(nn.ReLU(True))
        self.nets = nn.Sequential(*models)
        self.out = nn.Conv2d(channels, 1, kernel, 1, kernel // 2)

    def forward(self, x):
        x = self.nets(x)
        x = self.out(x)
        return x


if __name__ == "__main__":
    x = torch.rand(3, 1, 16, 16)
    model = EnergyModel()
    y = model(x)
    loss = nn.CrossEntropyLoss()
    print(x[:, 0, :, :].long().shape)
    print(y.shape)
    print(loss(y, x[:, 0, :, :].long()))
