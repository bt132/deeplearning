from model import EnergyModel
import torchvision
import torch.nn as nn
import torch
from torch.autograd import Variable
from torch import optim
import os
from metropolis import sample_metropolis_hasting

if __name__ == "__main__":
    device = "cuda"
    epoch = 24
    batch = 128
    model = EnergyModel().to(device)
    ds_USPS = torchvision.datasets.USPS(
        "USPS",
        train=True,
        download=True,
        transform=torchvision.transforms.Compose(
            [torchvision.transforms.Resize(16), torchvision.transforms.ToTensor()]
        ),
    )
    dataloader = torch.utils.data.DataLoader(ds_USPS, batch_size=batch)
    loss_fn = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters())
    save_path = os.getcwd() + "/models"
    for e in range(1, epoch + 1):
        print(f"---epoch {e} ---")
        for batch, (X, _) in enumerate(dataloader):
            X = X.to(device)
            optimizer.zero_grad()
            output = model(X)

            sample_x = torch.randn_like(X)
            sample_x = sample_metropolis_hasting(sample_x, 100, model, device)

            output_sample = model(sample_x)

            loss = loss_fn(output, X) + loss_fn(output_sample, X)
            loss.backward()
            optimizer.step()

            if batch % 20 == 0:
                print(f"loss at batch {batch} / {len(dataloader)}: {loss.item()}")

        if not os.path.exists(save_path):
            os.makedirs(save_path)
        if e == epoch:
            torch.save(
                model.state_dict(), save_path + "/Model_Checkpoint_" + "Last" + ".pt"
            )
        else:
            torch.save(
                model.state_dict(), save_path + "/Model_Checkpoint_" + str(e) + ".pt"
            )
        print("model saved")
    print("training done!")
    pass
